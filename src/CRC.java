import java.util.Scanner;

public class CRC {
    int dados [];
    int polinomio [];               // polinômio gerador
    int sequencia;                  // quantidade da sequencia de bits = 3
    int bitsAdd [];                 // bits adicionais
    int emissor [];
    int resto [];
    int polinomioZero [] = {0,0,0,0};
    int cadeiaBits [];
    int dado;
    String dados_bits;
    String polinomio_bits;
    int repeticao;
    
    Scanner leia = new Scanner (System.in);
    
    public void comecaPrograma (){
        leia = new Scanner(System.in);
        
        System.out.print("Insira os dados (cadeia de bits): ");
        dados_bits = leia.next();
        String[] cadeias = new String[dados_bits.length()];
        
        System.out.print("Insira o polinômio: ");
        polinomio_bits = leia.next();
        String[] cadeia_polinomio = new String[polinomio_bits.length()];
        
        
        // ----------------------------------------------- inicialização das variáveis globais
        dados = new int [dados_bits.length()];
        polinomio = new int[polinomio_bits.length()];
        sequencia = polinomio.length - 1;
        bitsAdd  = new int [sequencia]; 
        emissor = new int [dados.length + sequencia];
        resto = new int [polinomio.length +1];
        cadeiaBits = new int [dados.length + sequencia]; 
        repeticao = emissor.length - polinomio.length;
        // -----------------------------------------------

        int x = 0;
        for (int i = 0; i < cadeias.length; i++) {  // pega cada caracter e adiciona no vetor de inteiro
            cadeias[i] = dados_bits.substring(x, x + 1);
            x++;
            int dado = Integer.parseInt(cadeias[i]);
            dados[i] = dado;
        }
        int y= 0;
        for (int i = 0; i < cadeia_polinomio.length; i++) { // pega cada caracter e adiciona no vetor de inteiro
            cadeia_polinomio[i] = polinomio_bits.substring(y, y + 1);
            y++;
            int dado = Integer.parseInt(cadeia_polinomio[i]);
            polinomio[i] = dado;
        }
        
        for (int i = 0; i < sequencia ; i++ ){ // adiciona a quantidade de bits adds no vetor
            bitsAdd[i] = 0;
        }
        
        System.out.println("\nBits Add:");
        
        for (int i = 0; i < sequencia ; i++) {
            System.out.print(bitsAdd[i]);
        }
        
        int ct=0;        
        for(int i = 0; i < emissor.length; i++){  // junta os dados com os bits adicionais
            if (i < dados.length){
                emissor[i]= dados[i];
            }else{
                emissor[i] = bitsAdd[ct];
                ct++;
            }
        }
        
        System.out.println("\nEmissor:");
        for (int i = 0; i < emissor.length; i++) {
            System.out.print(emissor[i]);
        }       
        System.out.println("\n\n----------  DIVISÃO   -----------");
        divisaoEmissor();
    }
    
    public void divisaoEmissor (){
        for (int i = 0; i < polinomio.length; i++) { //divisão dos bits
            if (emissor[i] == polinomio[i]) // se forem iguais resto = 0
                resto[i] = 0;
            else if (emissor[i] != polinomio[i]) // se forem diferentes resto = 1
                    resto[i] = 1;
        }
        
        int desce = 1;
        System.out.println("\nResto:");
        resto[resto.length -1] = emissor[polinomio.length+desce-1]; // bit que desce
        
        for (int i = 0; i < resto.length; i++) {
            System.out.print(resto[i]);             // mostra resto
        }
        
        do{  
            int ct = 1;

            if(resto[1] == 0){                  // verifica se o primeiro bit é zero
                restoZero();
            } else {
                for (int i = 0; i < polinomio.length; i++) { // faz a divisão novamente
                    if (resto[ct] == polinomio[i]){         // se forem iguais resto = 0
                        resto[i] = 0;
                        ct++;
                    }else if (resto[ct] != polinomio[i]){   // se forem diferentes resto = 1
                        resto[i] = 1;
                        ct++;
                    }
                }
            }

            System.out.println("\nResto:");
            
            if(desce < repeticao){ 
                desce++;
                resto[resto.length -1] = emissor[polinomio.length+desce-1]; // bit que desce
            }
            for (int i = 0; i < resto.length; i++) {
                System.out.print(resto[i]);
            }
            
        }while(desce < repeticao);
        
        int ct = 1;
        if(resto[1] == 0){
            restoZero();
        } else {
            for (int i = 0; i < polinomio.length; i++) { // ultima divisão
                if (resto[ct] == polinomio[i]){         // se forem iguais resto = 0
                    resto[i] = 0;
                    ct++;
                }else if (resto[ct] != polinomio[i]){   // se forem diferentes resto = 1
                    resto[i] = 1;
                    ct++;
                }
            }
        }
        System.out.println("\nResto:");
           
        for (int i = 0; i < resto.length -1; i++) {
            System.out.print(resto[i]);
        }
        System.out.println("\nCadeia de Bits(EMISSOR):");
        
        int c = 1;
        for (int i = 0; i < emissor.length; i++) { // mostra os dados com a adição do resto (bits adds)
            if (i < dados.length){
                emissor[i] = dados[i];
                System.out.print(emissor[i]);
            }else{
                emissor[i] = resto[c];
                c++;
                System.out.print(emissor[i]);
            }
        }
        System.out.println("");
        
        divisaoReceptor();
    }
    public void divisaoReceptor (){
        
        System.out.println("\n---------------------------------------");
        
        for (int i = 0; i < polinomio.length; i++) {
            if (emissor[i] == polinomio[i]) // se forem iguais resto = 0
                resto[i] = 0;
            else if (emissor[i] != polinomio[i]) // se forem diferentes resto = 1
                    resto[i] = 1;
        }
        
        int desce = 1;
        System.out.println("\nResto:");
        resto[resto.length -1] = emissor[polinomio.length+desce-1]; // bit que desce
        
        for (int i = 0; i < resto.length; i++) {
            System.out.print(resto[i]);
        }
        
        do{  
            int ct = 1;

            if(resto[1] == 0){
                restoZero();
            } else {
                for (int i = 0; i < polinomio.length; i++) {
                    if (resto[ct] == polinomio[i]){ // se forem iguais resto = 0
                        resto[i] = 0;
                        ct++;
                    }else if (resto[ct] != polinomio[i]){ // se forem diferentes resto = 1
                        resto[i] = 1;
                        ct++;
                    }
                }
            }

            System.out.println("\nResto:");
            
            if(desce < repeticao){
                desce++;
                resto[resto.length -1] = emissor[polinomio.length+desce-1]; // bit que desce
            }
            for (int i = 0; i < resto.length; i++) {
                System.out.print(resto[i]);
            }
        
        }while(desce < repeticao);
        
        int ct = 1;
        if(resto[1] == 0){
            restoZero();
        } else {
            for (int i = 0; i < polinomio.length; i++) {
                if (resto[ct] == polinomio[i]){ // se forem iguais resto = 0
                    resto[i] = 0;
                    ct++;
                }else if (resto[ct] != polinomio[i]){ // se forem diferentes resto = 1
                    resto[i] = 1;
                    ct++;
                }
            }
        }
        System.out.println("\nResto:");
           
        int res = 0;
        for (int i = 0; i < resto.length -1; i++) {
            System.out.print(resto[i]);
            if(resto[i] == 0){
                res++;
            }
        }
        
        if(res == 4)
            System.out.println("\n\nNÃO HOUVE FALHA NA TRANSMISSÃO");
        else 
            System.out.println("\nHOUVE FALHA NA TRANSMISSÃO");
        
        System.out.println("\nCadeia de bits(RECEPTOR):");
        
        int c = 1;
        for (int i = 0; i < emissor.length; i++) {
            if (i < dados.length){
                emissor[i] = dados[i];
                System.out.print(emissor[i]);
            }else{
                emissor[i] = resto[c];
                c++;
                System.out.print(emissor[i]);
            }
        }
        System.out.println("");
        
    }
    public void restoZero (){
        int ct = 1;
        for (int i = 0; i < polinomio.length; i++) {
            if (resto[ct] == polinomioZero[i]){ // se forem iguais resto = 0
                resto[i] = 0;
                ct++;
            }else if (resto[ct] != polinomioZero[i]){ // se forem diferentes resto = 1
                resto[i] = 1;
                ct++;
            }
        }   
    }
}
