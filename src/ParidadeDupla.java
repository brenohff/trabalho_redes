
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author breno
 */
public class ParidadeDupla {

    Scanner leia;
    private String cadeia_bits_emissor;
    private String cadeia_bits_receptor;

    public void emissor() {

        leia = new Scanner(System.in);
        String cadeia_bits;

        System.out.print("Insira uma cadeia de bits: ");
        cadeia_bits = leia.next();
        cadeia_bits_emissor = cadeia_bits;
        String[] cadeias = new String[cadeia_bits.length() / 7];

        if (cadeia_bits.length() % 2 != 0) {
            System.out.print("Insira uma cadeia com número PAR de bits =D");
        } else {
            int x = 0;
            for (int i = 0; i < cadeias.length; i++) {
                cadeias[i] = cadeia_bits.substring(x, x + 7);
                x += 7;
            }
        }

        System.out.println("");
        checaMensagem(adicionaBitsParidade(cadeias));
    }

    private String adicionaBitsParidade(String[] cadeias) {

        String[] new_cadeias = new String[cadeias.length + 1];
        String new_cadeia_bits = "";

        for (int i = 0; i < new_cadeias.length; i++) {
            int qtd_1 = 0;

            if (i < (new_cadeias.length - 1)) {

                for (int j = 0; j < cadeias[i].length(); j++) {
                    if (cadeias[i].charAt(j) == '1') {
                        qtd_1++;
                    }

                    if (qtd_1 % 2 == 0) {
                        new_cadeias[i] = cadeias[i].concat("0");
                    } else {
                        new_cadeias[i] = cadeias[i].concat("1");
                    }
                }

                for (int j = 0; j < new_cadeias[i].length(); j++) {
                    if (j < new_cadeias[i].length() - 1) {
                        System.out.print(new_cadeias[i].charAt(j));
                    } else {
                        System.out.print("|" + new_cadeias[i].charAt(j) + "\n");
                    }
                }

            } else {

                new_cadeias[i] = "";

                for (int j = 0; j < 8; j++) {
                    int[] qtd_1_1 = new int[8];

                    for (int k = 0; k < cadeias.length; k++) {
                        if (new_cadeias[k].charAt(j) == '1') {
                            qtd_1_1[j]++;
                        }
                    }

                    if (qtd_1_1[j] % 2 != 0) {
                        new_cadeias[i] = new_cadeias[i].concat("1");
                    } else {
                        new_cadeias[i] = new_cadeias[i].concat("0");
                    }
                }
                System.out.println("---------");
                for (int j = 0; j < new_cadeias[i].length(); j++) {
                    if (j < new_cadeias[i].length() - 1) {
                        System.out.print(new_cadeias[i].charAt(j));
                    } else {
                        System.out.print("|" + new_cadeias[i].charAt(j) + "\n");
                    }
                }

            }
        }

        for (int i = 0; i < new_cadeias.length; i++) {
            new_cadeia_bits = new_cadeia_bits.concat(new_cadeias[i]);
        }

        System.out.println("\nNova cadeia com os bits de paridade (EMISSOR): " + new_cadeia_bits + "\n\nEnviando para o receptor....\n");

        return new_cadeia_bits;
    }

    private void receptor(String new_cadeia_bits) {
        String[] cadeias_de_bits = new String[new_cadeia_bits.length() / 8];

        boolean b = true;
        
        int y = 0;
        for (int i = 0; i < cadeias_de_bits.length; i++) {

            cadeias_de_bits[i] = new_cadeia_bits.substring(y, y + 8);
            y += 8;;

            if (i < (cadeias_de_bits.length - 1)) {

                int x = 0;
                for (int j = 0; j < cadeias_de_bits[i].length(); j++) {
                    if (cadeias_de_bits[i].charAt(j) == '1') {
                        x++;
                    }

                    if (j == (cadeias_de_bits[i].length() - 2)) {
                        if ((x % 2) != 0 && cadeias_de_bits[i].charAt(cadeias_de_bits[i].length() - 1) == '0') {
                            b = false;
                        }

                        if ((x % 2) == 0 && cadeias_de_bits[i].charAt(cadeias_de_bits[i].length() - 1) == '1') {
                            b = false;
                        }
                    }
                }
            } else if (i == (cadeias_de_bits.length - 1)) {
                for (int j = 0; j < 8; j++) {
                    
                    int ct = 0;
                    for (int k = 0; k < cadeias_de_bits.length; k++) {
                        if(cadeias_de_bits[k].charAt(j) == '1'){
                            ct++;
                        }
                        
                        if(k == (cadeias_de_bits.length - 1)){
                            if(ct % 2 == 0 && cadeias_de_bits[k].charAt(j) == '1'){
                                b = false;
                            }
                            
                            if(ct % 2 != 0 && cadeias_de_bits[k].charAt(j) == '0'){
                                b = false;
                            }
                        }
                    }
                    
                }
            }

        }
        
        if(!b){
            System.out.println("Houve erro na mensagem");
        }else{
            System.out.println("Não houve erro");
        }

    }

    private void checaMensagem(String msg_emissor) {
        leia = new Scanner(System.in);

        System.out.println("Deseja simular algum erro? 1- Sim ou 2- Não");
        int escolha = leia.nextInt();

        switch (escolha) {
            case 1:
                receptor(simulaErro(msg_emissor));
                break;
            case 2:
                receptor(msg_emissor);
                break;
        }

    }

    private String simulaErro(String msg_emissor) {
        Random random = new Random();

        int x1 = random.nextInt(msg_emissor.length());
        int x2 = random.nextInt(msg_emissor.length());
        int x3 = random.nextInt(msg_emissor.length());
        String msg_com_erro = "";

        for (int i = 0; i < msg_emissor.length(); i++) {
            if (i == x1 || i == x2 || i == x3) {
                if (msg_emissor.charAt(i) == '1') {
                    msg_com_erro = msg_com_erro.concat("0");
                } else {
                    msg_com_erro = msg_com_erro.concat("1");
                }
            } else {
                if (msg_emissor.charAt(i) == '1') {
                    msg_com_erro = msg_com_erro.concat("1");
                } else {
                    msg_com_erro = msg_com_erro.concat("0");
                }
            }
        }

        String[] cadeia = new String[msg_com_erro.length() / 8];

        int y = 0;
        for (int i = 0; i < cadeia.length; i++) {
            cadeia[i] = msg_com_erro.substring(y, y + 8);
            y += 8;
        }

        for (int i = 0; i < cadeia.length; i++) {
            if (i < (cadeia.length - 1)) {

                for (int j = 0; j < cadeia[i].length(); j++) {
                    if (j < cadeia[i].length() - 1) {
                        System.out.print(cadeia[i].charAt(j));
                    } else {
                        System.out.print("|" + cadeia[i].charAt(j) + "\n");
                    }
                }
            } else {
                System.out.println("---------");
                for (int j = 0; j < cadeia[i].length(); j++) {
                    if (j < cadeia[i].length() - 1) {
                        System.out.print(cadeia[i].charAt(j));
                    } else {
                        System.out.print("|" + cadeia[i].charAt(j) + "\n");
                    }
                }
            }

        }

        System.out.println(msg_emissor + "\n" + msg_com_erro);
        return msg_com_erro;

    }
}
